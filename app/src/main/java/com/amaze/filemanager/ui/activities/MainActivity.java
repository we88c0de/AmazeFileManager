/*
 * Copyright (C) 2014-2020 Arpit Khurana <arpitkh96@gmail.com>, Vishal Nehra <vishalmeham2@gmail.com>,
 * Emmanuel Messulam<emmanuelbendavid@gmail.com>, Raymond Lai <airwave209gt at gmail.com> and Contributors.
 *
 * This file is part of Amaze File Manager.
 *
 * Amaze File Manager is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.amaze.filemanager.ui.activities;

import static android.os.Build.VERSION.SDK_INT;
import static com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants.PREFERENCE_BOOKMARKS_ADDED;
import static com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants.PREFERENCE_COLORED_NAVIGATION;
import static com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants.PREFERENCE_NEED_TO_SET_HOME;
import static com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants.PREFERENCE_VIEW;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.folderselector.FolderChooserDialog;
import com.amaze.filemanager.LogHelper;
import com.amaze.filemanager.R;
import com.amaze.filemanager.TagsHelper;
import com.amaze.filemanager.adapters.data.StorageDirectoryParcelable;
import com.amaze.filemanager.application.AppConfig;
import com.amaze.filemanager.asynchronous.asynctasks.DeleteTask;
import com.amaze.filemanager.database.TabHandler;
import com.amaze.filemanager.database.UtilsHandler;
import com.amaze.filemanager.database.models.OperationData;
import com.amaze.filemanager.filesystem.FileUtil;
import com.amaze.filemanager.filesystem.HybridFile;
import com.amaze.filemanager.filesystem.HybridFileParcelable;
import com.amaze.filemanager.filesystem.PasteHelper;
import com.amaze.filemanager.filesystem.StorageNaming;
import com.amaze.filemanager.filesystem.files.FileUtils;
import com.amaze.filemanager.filesystem.usb.SingletonUsbOtg;
import com.amaze.filemanager.filesystem.usb.UsbOtgRepresentation;
import com.amaze.filemanager.ui.activities.superclasses.PermissionsActivity;
import com.amaze.filemanager.ui.colors.ColorPreferenceHelper;
import com.amaze.filemanager.ui.dialogs.GeneralDialogCreation;
import com.amaze.filemanager.ui.dialogs.RenameBookmark;
import com.amaze.filemanager.ui.dialogs.RenameBookmark.BookmarkCallback;
import com.amaze.filemanager.ui.fragments.AppsListFragment;
import com.amaze.filemanager.ui.fragments.MainFragment;
import com.amaze.filemanager.ui.fragments.ProcessViewerFragment;
import com.amaze.filemanager.ui.fragments.SearchWorkerFragment;
import com.amaze.filemanager.ui.fragments.TabFragment;
import com.amaze.filemanager.ui.fragments.preference_fragments.PreferencesConstants;
import com.amaze.filemanager.ui.theme.AppTheme;
import com.amaze.filemanager.ui.views.appbar.AppBar;
import com.amaze.filemanager.ui.views.drawer.Drawer;
import com.amaze.filemanager.utils.AppConstants;
import com.amaze.filemanager.utils.BookSorter;
import com.amaze.filemanager.utils.DataUtils;
import com.amaze.filemanager.utils.DataUtils.DataChangeListener;
import com.amaze.filemanager.utils.MainActivityHelper;
import com.amaze.filemanager.utils.OTGUtil;
import com.amaze.filemanager.utils.OpenMode;
import com.amaze.filemanager.utils.PreferenceUtils;
import com.amaze.filemanager.utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.usb.UsbManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import eu.chainfire.libsuperuser.Shell;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends PermissionsActivity
    implements
        DataChangeListener,
        BookmarkCallback,
        SearchWorkerFragment.HelperCallbacks,
        LoaderManager.LoaderCallbacks<Cursor>,
        FolderChooserDialog.FolderCallback {

  private static final String TAG = TagsHelper.getTag(MainActivity.class);

  public static final Pattern DIR_SEPARATOR = Pattern.compile("/");
  public static final String TAG_ASYNC_HELPER = "async_helper";

  private DataUtils dataUtils;

  public String path = "";
  public boolean mReturnIntent = false;
  public boolean openzip = false;
  public boolean mRingtonePickerIntent = false;
  public int skinStatusBar;


  public MainActivityHelper mainActivityHelper;

  public int operation = -1;
  public ArrayList<HybridFileParcelable> oparrayList;
  public ArrayList<ArrayList<HybridFileParcelable>> oparrayListList;

  // oppathe - the path at which certain operation needs to be performed
  // oppathe1 - the new path which user wants to create/modify
  // oppathList - the paths at which certain operation needs to be performed (pairs with
  // oparrayList)
  public String oppathe, oppathe1;
  public ArrayList<String> oppatheList;

  // This holds the Uris to be written at initFabToSave()
  private ArrayList<Uri> urisToBeSaved;

  public static final String PASTEHELPER_BUNDLE = "pasteHelper";

  private static final String KEY_DRAWER_SELECTED = "selectitem";
  private static final String KEY_OPERATION_PATH = "oppathe";
  private static final String KEY_OPERATED_ON_PATH = "oppathe1";
  private static final String KEY_OPERATIONS_PATH_LIST = "oparraylist";
  private static final String KEY_OPERATION = "operation";

  private AppBar appbar;
  private Drawer drawer;
  // private HistoryManager history, grid;
  private MainActivity mainActivity = this;
  private String zippath;
  private boolean openProcesses = false;
  private MaterialDialog materialDialog;
  private boolean backPressedToExitOnce = false;
  private Toast toast = null;
  private Intent intent;
  private View indicator_layout;

  private AppBarLayout appBarLayout;

  private UtilsHandler utilsHandler;
  /**
   * This is for a hack.
   *
   * @see MainActivity#onLoadFinished(Loader, Cursor)
   */
  private Cursor cloudCursorData = null;

  public static final int REQUEST_CODE_SAF = 223;

  public static final String KEY_INTENT_PROCESS_VIEWER = "openprocesses";
  public static final String TAG_INTENT_FILTER_FAILED_OPS = "failedOps";
  public static final String TAG_INTENT_FILTER_GENERAL = "general_communications";
  public static final String ARGS_KEY_LOADER = "loader_cloud_args_service";

  /**
   * Broadcast which will be fired after every file operation, will denote list loading Registered
   * by {@link MainFragment}
   */
  public static final String KEY_INTENT_LOAD_LIST = "loadlist";

  /**
   * Extras carried by the list loading intent Contains path of parent directory in which operation
   * took place, so that we can run media scanner on it
   */
  public static final String KEY_INTENT_LOAD_LIST_FILE = "loadlist_file";

  /**
   * Mime type in intent that apps need to pass when trying to open file manager from a specific
   * directory Should be clubbed with {@link Intent#ACTION_VIEW} and send in path to open in intent
   * data field
   */
  public static final String ARGS_INTENT_ACTION_VIEW_MIME_FOLDER = "resource/folder";

  public static final String CLOUD_AUTHENTICATOR_GDRIVE = "android.intent.category.BROWSABLE";
  public static final String CLOUD_AUTHENTICATOR_REDIRECT_URI = "com.amaze.filemanager:/auth";

  // the current visible tab, either 0 or 1
  public static int currentTab;

  public static Shell.Interactive shellInteractive;
  public static Handler handler;

  private static HandlerThread handlerThread;

  public static final int REQUEST_CODE_CLOUD_LIST_KEYS = 5463;
  public static final int REQUEST_CODE_CLOUD_LIST_KEY = 5472;

  private PasteHelper pasteHelper;

  private static final String DEFAULT_FALLBACK_STORAGE_PATH = "/storage/sdcard0";
  private static final String INTERNAL_SHARED_STORAGE = "Internal shared storage";

  /** Called when the activity is first created. */
  @Override
  public void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    dataUtils = DataUtils.getInstance();

    initialisePreferences();
    initializeInteractiveShell();

    dataUtils.registerOnDataChangedListener(this);

    AppConfig.getInstance().setMainActivityContext(this);

    setContentView(R.layout.main_toolbar);
    appbar =
        new AppBar(
            this,
            getPrefs(),
            queue -> {
              if (!queue.isEmpty()) {
                mainActivityHelper.search(getPrefs(), queue);
              }
            });
    initialiseViews();
    utilsHandler = AppConfig.getInstance().getUtilsHandler();

    mainActivityHelper = new MainActivityHelper(this);

    path = getIntent().getStringExtra("path");
    openProcesses = getIntent().getBooleanExtra(KEY_INTENT_PROCESS_VIEWER, false);
    intent = getIntent();

    if (intent.getStringArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS) != null) {
      ArrayList<HybridFileParcelable> failedOps =
          intent.getParcelableArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS);
      if (failedOps != null) {
        mainActivityHelper.showFailedOperationDialog(failedOps, this);
      }
    }

    checkForExternalIntent(intent);

    // setting window background color instead of each item, in order to reduce pixel overdraw
    if (getAppTheme().equals(AppTheme.LIGHT)) {
      /*if(Main.IS_LIST)
          getWindow().setBackgroundDrawableResource(android.R.color.white);
      else
          getWindow().setBackgroundDrawableResource(R.color.grid_background_light);
      */
      getWindow().setBackgroundDrawableResource(android.R.color.white);
    } else if (getAppTheme().equals(AppTheme.BLACK)) {
      getWindow().setBackgroundDrawableResource(android.R.color.black);
    } else {
      getWindow().setBackgroundDrawableResource(R.color.holo_dark_background);
    }

    /*findViewById(R.id.drawer_buttton).setOnClickListener(new ImageView.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mDrawerLayout.isOpen(mDrawerLinear)) {
                mDrawerLayout.close(mDrawerLinear);
            } else mDrawerLayout.openDrawer(mDrawerLinear);
        }
    });*/

    drawer.setDrawerIndicatorEnabled();

    // recents header color implementation
    if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      ActivityManager.TaskDescription taskDescription =
          new ActivityManager.TaskDescription(
              "Amaze",
              ((BitmapDrawable) getResources().getDrawable(R.mipmap.ic_launcher)).getBitmap(),
              ColorPreferenceHelper.getPrimary(
                  getCurrentColorPreference(), MainActivity.currentTab));
      setTaskDescription(taskDescription);
    }

    if (!getBoolean(PREFERENCE_BOOKMARKS_ADDED)) {
      utilsHandler.addCommonBookmarks();
      getPrefs().edit().putBoolean(PREFERENCE_BOOKMARKS_ADDED, true).commit();
    }

    checkForExternalPermission();

    Completable.fromRunnable(
            () -> {
              dataUtils.setHiddenFiles(utilsHandler.getHiddenFilesConcurrentRadixTree());
              dataUtils.setHistory(utilsHandler.getHistoryLinkedList());
              dataUtils.setGridfiles(utilsHandler.getGridViewList());
              dataUtils.setListfiles(utilsHandler.getListViewList());
              dataUtils.setBooks(utilsHandler.getBookmarksList());
              ArrayList<String[]> servers = new ArrayList<>();
              servers.addAll(utilsHandler.getSmbList());
              dataUtils.setServers(servers);
            })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            new CompletableObserver() {
              @Override
              public void onSubscribe(Disposable d) {}

              @Override
              public void onComplete() {
                drawer.refreshDrawer();
                invalidateFragmentAndBundle(savedInstanceState);
              }

              @Override
              public void onError(Throwable e) {
                e.printStackTrace();
              }
            });
  }

  private void invalidateFragmentAndBundle(Bundle savedInstanceState) {
    if (savedInstanceState == null) {
      if (openProcesses) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(
            R.id.content_frame, new ProcessViewerFragment(), KEY_INTENT_PROCESS_VIEWER);
        // transaction.addToBackStack(null);
        openProcesses = false;
        // title.setText(utils.getString(con, R.string.process_viewer));
        // Commit the transaction
        transaction.commit();
        supportInvalidateOptionsMenu();
      } else {
        if (path != null && path.length() > 0) {
          HybridFile file = new HybridFile(OpenMode.UNKNOWN, path);
          file.generateMode(MainActivity.this);
          if (file.isDirectory(MainActivity.this)) goToMain(path);
          else {
            goToMain(null);
            FileUtils.openFile(new File(path), MainActivity.this, getPrefs());
          }
        } else {
          goToMain(null);
        }
      }
    } else {
      pasteHelper = savedInstanceState.getParcelable(PASTEHELPER_BUNDLE);
      oppathe = savedInstanceState.getString(KEY_OPERATION_PATH);
      oppathe1 = savedInstanceState.getString(KEY_OPERATED_ON_PATH);
      oparrayList = savedInstanceState.getParcelableArrayList(KEY_OPERATIONS_PATH_LIST);
      operation = savedInstanceState.getInt(KEY_OPERATION);
      int selectedStorage = savedInstanceState.getInt(KEY_DRAWER_SELECTED, 0);
      getDrawer().selectCorrectDrawerItem(selectedStorage);
    }
  }

  private void checkForExternalPermission() {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !checkStoragePermission()) {
      requestStoragePermission(
          () -> {
            drawer.refreshDrawer();
            TabFragment tabFragment = getTabFragment();
            boolean b = getBoolean(PREFERENCE_NEED_TO_SET_HOME);
            // reset home and current paths according to new storages
            if (b) {
              TabHandler tabHandler = TabHandler.getInstance();
              tabHandler.clear();
              if (tabFragment != null) {
                tabFragment.refactorDrawerStorages(false);
                Fragment main = tabFragment.getFragmentAtIndex(0);
                if (main != null) ((MainFragment) main).updateTabWithDb(tabHandler.findTab(1));
                Fragment main1 = tabFragment.getFragmentAtIndex(1);
                if (main1 != null) ((MainFragment) main1).updateTabWithDb(tabHandler.findTab(2));
              }
              getPrefs().edit().putBoolean(PREFERENCE_NEED_TO_SET_HOME, false).commit();
            } else {
              // just refresh list
              if (tabFragment != null) {
                Fragment main = tabFragment.getFragmentAtIndex(0);
                if (main != null) ((MainFragment) main).updateList();
                Fragment main1 = tabFragment.getFragmentAtIndex(1);
                if (main1 != null) ((MainFragment) main1).updateList();
              }
            }
          },
          true);
    }
  }

  /** Checks for the action to take when Amaze receives an intent from external source */
  private void checkForExternalIntent(Intent intent) {
    String actionIntent = intent.getAction();
    String type = intent.getType();

    if (actionIntent != null) {
      if (actionIntent.equals(Intent.ACTION_GET_CONTENT)) {
        // file picker intent
        mReturnIntent = true;
        Toast.makeText(this, getString(R.string.pick_a_file), Toast.LENGTH_LONG).show();

        // disable screen rotation just for convenience purpose
        // TODO: Support screen rotation when picking file
        Utils.disableScreenRotation(this);
      } else if (actionIntent.equals(RingtoneManager.ACTION_RINGTONE_PICKER)) {
        // ringtone picker intent
        mReturnIntent = true;
        mRingtonePickerIntent = true;
        Toast.makeText(this, getString(R.string.pick_a_file), Toast.LENGTH_LONG).show();

        // disable screen rotation just for convenience purpose
        // TODO: Support screen rotation when picking file
        Utils.disableScreenRotation(this);
      } else if (actionIntent.equals(Intent.ACTION_VIEW)) {
        // zip viewer intent
        Uri uri = intent.getData();

        if (type != null && type.equals(ARGS_INTENT_ACTION_VIEW_MIME_FOLDER)) {
          // support for syncting or intents from external apps that
          // need to start file manager from a specific path

          if (uri != null) {

            path = Utils.sanitizeInput(uri.getPath());
          } else {
            // no data field, open home for the tab in later processing
            path = null;
          }
        } else {
          // we don't have folder resource mime type set, supposed to be zip/rar
          openzip = true;
          zippath = Utils.sanitizeInput(uri.toString());
        }

      } else if (actionIntent.equals(Intent.ACTION_SEND)) {
        if (type.equals("text/plain")) {
          initFabToSave(null);
        } else {
          // save a single file to filesystem
          Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
          ArrayList<Uri> uris = new ArrayList<>();
          uris.add(uri);
          initFabToSave(uris);
        }
        // disable screen rotation just for convenience purpose
        // TODO: Support screen rotation when saving a file
        Utils.disableScreenRotation(this);

      } else if (actionIntent.equals(Intent.ACTION_SEND_MULTIPLE) && type != null) {
        // save multiple files to filesystem

        ArrayList<Uri> arrayList = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        initFabToSave(arrayList);

        // disable screen rotation just for convenience purpose
        // TODO: Support screen rotation when saving a file
        Utils.disableScreenRotation(this);
      }
    }
  }

  /** Initializes the floating action button to act as to save data from an external intent */
  private void initFabToSave(final ArrayList<Uri> uris) {
    Utils.showThemedSnackbar(
        this,
        getString(R.string.select_save_location),
        BaseTransientBottomBar.LENGTH_INDEFINITE,
        R.string.save,
        () -> saveExternalIntent(uris));
  }

  private void saveExternalIntent(final ArrayList<Uri> uris) {
    if (uris != null && uris.size() > 0) {
      if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        File folder = new File(getCurrentMainFragment().getCurrentPath());
        int result = mainActivityHelper.checkFolder(folder, MainActivity.this);
        if (result == MainActivityHelper.WRITABLE_OR_ON_SDCARD) {
          FileUtil.writeUriToStorage(
              MainActivity.this,
              uris,
              getContentResolver(),
              getCurrentMainFragment().getCurrentPath());
          finish();
        } else {
          // Trigger SAF intent, keep uri until finish
          operation = DataUtils.SAVE_FILE;
          urisToBeSaved = uris;
          mainActivityHelper.checkFolder(folder, MainActivity.this);
        }
      } else {
        FileUtil.writeUriToStorage(
            MainActivity.this,
            uris,
            getContentResolver(),
            getCurrentMainFragment().getCurrentPath());
      }
    } else {
      saveExternalIntentExtras();
    }
    Toast.makeText(
            MainActivity.this,
            getResources().getString(R.string.saving)
                + " to "
                + getCurrentMainFragment().getCurrentPath(),
            Toast.LENGTH_LONG)
        .show();
    finish();
  }

  private void saveExternalIntentExtras() {
    Bundle extras = intent.getExtras();
    StringBuilder data = new StringBuilder();
    if (!Utils.isNullOrEmpty(extras.getString(Intent.EXTRA_SUBJECT))) {
      data.append(extras.getString(Intent.EXTRA_SUBJECT));
    }
    if (!Utils.isNullOrEmpty(extras.getString(Intent.EXTRA_TEXT))) {
      data.append(AppConstants.NEW_LINE).append(extras.getString(Intent.EXTRA_TEXT));
    }
    String fileName = Long.toString(System.currentTimeMillis());

  }

  /**
   * Initializes an interactive shell, which will stay throughout the app lifecycle The shell is
   * associated with a handler thread which maintain the message queue from the callbacks of shell
   * as we certainly cannot allow the callbacks to run on same thread because of possible deadlock
   * situation and the asynchronous behaviour of LibSuperSU
   */
  private void initializeInteractiveShell() {
    // only one looper can be associated to a thread. So we are making sure not to create new
    // handler threads every time the code relaunch.
    if (isRootExplorer()) {
      handlerThread = new HandlerThread("handler");
      handlerThread.start();
      handler = new Handler(handlerThread.getLooper());
      shellInteractive = (new Shell.Builder()).useSU().setHandler(handler).open();

      // TODO: check for busybox
      /*try {
          if (!RootUtils.isBusyboxAvailable()) {
              Toast.makeText(this, getString(R.string.error_busybox), Toast.LENGTH_LONG).show();
              closeInteractiveShell();
              sharedPref.edit().putBoolean(PreferenceUtils.KEY_ROOT, false).apply();
          }
      } catch (ShellNotRunningException e) {
          e.printStackTrace();
          sharedPref.edit().putBoolean(PreferenceUtils.KEY_ROOT, false).apply();
      }*/
    }
  }

  /** @return paths to all available volumes in the system (include emulated) */
  public synchronized ArrayList<StorageDirectoryParcelable> getStorageDirectories() {
    ArrayList<StorageDirectoryParcelable> volumes;
    if (SDK_INT >= Build.VERSION_CODES.N) {
      volumes = getStorageDirectoriesNew();
    } else {
      volumes = getStorageDirectoriesLegacy();
    }
    if (isRootExplorer()) {
      volumes.add(
          new StorageDirectoryParcelable(
              "/",
              getResources().getString(R.string.root_directory),
              R.drawable.ic_drawer_root_white));
    }
    return volumes;
  }

  /**
   * @return All available storage volumes (including internal storage, SD-Cards and USB devices)
   */
  @TargetApi(Build.VERSION_CODES.N)
  public synchronized ArrayList<StorageDirectoryParcelable> getStorageDirectoriesNew() {
    // Final set of paths
    ArrayList<StorageDirectoryParcelable> volumes = new ArrayList<>();
    StorageManager sm = getSystemService(StorageManager.class);
    for (StorageVolume volume : sm.getStorageVolumes()) {
      if (!volume.getState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)
          && !volume.getState().equalsIgnoreCase(Environment.MEDIA_MOUNTED_READ_ONLY)) {
        continue;
      }
      File path = Utils.getVolumeDirectory(volume);
      String name = volume.getDescription(this);
      if (INTERNAL_SHARED_STORAGE.equalsIgnoreCase(name)) {
        name = getString(R.string.storage_internal);
      }
      int icon;
      if (!volume.isRemovable()) {
        icon = R.drawable.ic_phone_android_white_24dp;
      } else {
        // HACK: There is no reliable way to distinguish USB and SD external storage
        // However it is often enough to check for "USB" String
        if (name.toUpperCase().contains("USB") || path.getPath().toUpperCase().contains("USB")) {
          icon = R.drawable.ic_usb_white_24dp;
        } else {
          icon = R.drawable.ic_sd_storage_white_24dp;
        }
      }
      volumes.add(new StorageDirectoryParcelable(path.getPath(), name, icon));
    }
    return volumes;
  }

  /**
   * Returns all available SD-Cards in the system (include emulated)
   *
   * <p>Warning: Hack! Based on Android source code of version 4.3 (API 18) Because there was no
   * standard way to get it before android N
   *
   * @return All available SD-Cards in the system (include emulated)
   */
  public synchronized ArrayList<StorageDirectoryParcelable> getStorageDirectoriesLegacy() {
    List<String> rv = new ArrayList<>();

    // Primary physical SD-CARD (not emulated)
    final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
    // All Secondary SD-CARDs (all exclude primary) separated by ":"
    final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
    // Primary emulated SD-CARD
    final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");
    if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
      // Device has physical external storage; use plain paths.
      if (TextUtils.isEmpty(rawExternalStorage)) {
        // EXTERNAL_STORAGE undefined; falling back to default.
        // Check for actual existence of the directory before adding to list
        if (new File(DEFAULT_FALLBACK_STORAGE_PATH).exists()) {
          rv.add(DEFAULT_FALLBACK_STORAGE_PATH);
        } else {
          // We know nothing else, use Environment's fallback
          rv.add(Environment.getExternalStorageDirectory().getAbsolutePath());
        }
      } else {
        rv.add(rawExternalStorage);
      }
    } else {
      // Device has emulated storage; external storage paths should have
      // userId burned into them.
      final String rawUserId;
      if (SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
        rawUserId = "";
      } else {
        final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        final String[] folders = DIR_SEPARATOR.split(path);
        final String lastFolder = folders[folders.length - 1];
        boolean isDigit = false;
        try {
          Integer.valueOf(lastFolder);
          isDigit = true;
        } catch (NumberFormatException ignored) {
        }
        rawUserId = isDigit ? lastFolder : "";
      }
      // /storage/emulated/0[1,2,...]
      if (TextUtils.isEmpty(rawUserId)) {
        rv.add(rawEmulatedStorageTarget);
      } else {
        rv.add(rawEmulatedStorageTarget + File.separator + rawUserId);
      }
    }
    // Add all secondary storages
    if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
      // All Secondary SD-CARDs splited into array
      final String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
      Collections.addAll(rv, rawSecondaryStorages);
    }
    if (SDK_INT >= Build.VERSION_CODES.M && checkStoragePermission()) rv.clear();
    if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
      String strings[] = FileUtil.getExtSdCardPathsForActivity(this);
      for (String s : strings) {
        File f = new File(s);
        if (!rv.contains(s) && FileUtils.canListFiles(f)) rv.add(s);
      }
    }
    File usb = getUsbDrive();
    if (usb != null && !rv.contains(usb.getPath())) rv.add(usb.getPath());

    if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
      if (SingletonUsbOtg.getInstance().isDeviceConnected()) {
        rv.add(OTGUtil.PREFIX_OTG + "/");
      }
    }

    // Assign a label and icon to each directory
    ArrayList<StorageDirectoryParcelable> volumes = new ArrayList<>();
    for (String file : rv) {
      File f = new File(file);
      @DrawableRes int icon;

      if ("/storage/emulated/legacy".equals(file)
          || "/storage/emulated/0".equals(file)
          || "/mnt/sdcard".equals(file)) {
        icon = R.drawable.ic_phone_android_white_24dp;
      } else if ("/storage/sdcard1".equals(file)) {
        icon = R.drawable.ic_sd_storage_white_24dp;
      } else if ("/".equals(file)) {
        icon = R.drawable.ic_drawer_root_white;
      } else {
        icon = R.drawable.ic_sd_storage_white_24dp;
      }

      String name = StorageNaming.getDeviceDescriptionLegacy(this, f);
      volumes.add(new StorageDirectoryParcelable(file, name, icon));
    }

    return volumes;
  }

  @Override
  public void onBackPressed() {
    if (!drawer.isLocked() && drawer.isOpen()) {
      drawer.close();
      return;
    }

    Fragment fragment = getFragmentAtFrame();
    if (getAppbar().getSearchView().isShown()) {
      // hide search view if visible, with an animation
      getAppbar().getSearchView().hideSearchView();
    } else if (fragment instanceof TabFragment) {
        getCurrentMainFragment().goBack();

    }  else {
      goToMain(null);
    }
  }

  public void invalidatePasteSnackbar(boolean showSnackbar) {
    if (pasteHelper != null) {
      pasteHelper.invalidateSnackbar(this, showSnackbar);
    }
  }

  public void exit() {
    if (backPressedToExitOnce) {
      finish();
      if (isRootExplorer()) {
        // TODO close all shells
      }
    } else {
      this.backPressedToExitOnce = true;
      showToast(getString(R.string.press_again));
      new Handler()
          .postDelayed(
              () -> {
                backPressedToExitOnce = false;
              },
              2000);
    }
  }

  public void goToMain(String path) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    // title.setText(R.string.app_name);
    TabFragment tabFragment = new TabFragment();
    if (path != null && path.length() > 0) {
      Bundle b = new Bundle();
      b.putString("path", path);
      tabFragment.setArguments(b);
    }
    transaction.replace(R.id.content_frame, tabFragment);
    // Commit the transaction
    transaction.addToBackStack("tabt" + 1);
    transaction.commitAllowingStateLoss();
    appbar.setTitle(null);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.activity_extra, menu);
    /*
    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
    searchView.setIconifiedByDefault(false);

    MenuItem search = menu.findItem(R.id.search);
    MenuItemCompat.setOnActionExpandListener(search, new MenuItemCompat.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            // Stretching the SearchView across width of the Toolbar
            toolbar.setContentInsetsRelative(0, 0);
            return true;
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            // Restoring
            toolbar.setContentInsetsRelative(TOOLBAR_START_INSET, 0);
            return true;
        }
    });
    */
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    MenuItem search = menu.findItem(R.id.search);
    Fragment fragment = getFragmentAtFrame();
    if (fragment instanceof TabFragment) {
      appbar.setTitle(R.string.appbar_name);

      try {
        MainFragment ma = getCurrentMainFragment();
        appbar
            .getBottomBar()
            .updatePath(
                ma.getCurrentPath(),
                ma.results,
                MainActivityHelper.SEARCH_TEXT,
                ma.openMode,
                ma.folder_count,
                ma.file_count,
                ma);
      } catch (Exception e) {
      }

      appbar.getBottomBar().setClickListener();

      search.setVisible(true);
      if (indicator_layout != null) indicator_layout.setVisibility(View.VISIBLE);
      menu.findItem(R.id.search).setVisible(true);
      menu.findItem(R.id.home).setVisible(true);
      menu.findItem(R.id.sort).setVisible(true);
      invalidatePasteSnackbar(true);
      findViewById(R.id.buttonbarframe).setVisibility(View.VISIBLE);
    } else if (fragment instanceof AppsListFragment
        || fragment instanceof ProcessViewerFragment
        ) {
      appBarLayout.setExpanded(true);
      if (indicator_layout != null) indicator_layout.setVisibility(View.GONE);
      findViewById(R.id.buttonbarframe).setVisibility(View.GONE);
      menu.findItem(R.id.search).setVisible(false);
      menu.findItem(R.id.home).setVisible(false);

      if (fragment instanceof ProcessViewerFragment) {
        menu.findItem(R.id.sort).setVisible(false);
      } else {
        menu.findItem(R.id.dsort).setVisible(false);
        menu.findItem(R.id.sortby).setVisible(false);
      }

      invalidatePasteSnackbar(false);
    }
    return super.onPrepareOptionsMenu(menu);
  }

  void showToast(String message) {
    if (this.toast == null) {
      // Create toast if found null, it would he the case of first call only
      this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
    } else if (this.toast.getView() == null) {
      // Toast not showing, so create new one
      this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
    } else {
      // Updating toast message is showing
      this.toast.setText(message);
    }

    // Showing toast finally
    this.toast.show();
  }

  void killToast() {
    if (this.toast != null) this.toast.cancel();
  }

  // called when the user exits the action mode
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // The action bar home/up action should open or close the drawer.
    // ActionBarDrawerToggle will take care of this.
    if (drawer.onOptionsItemSelected(item)) return true;

    // Handle action buttons
    MainFragment ma = getCurrentMainFragment();

    switch (item.getItemId()) {
      case R.id.home:
        if (ma != null) ma.home();
        break;
      case R.id.exit:
        finish();
        break;
      case R.id.sort:
        Fragment fragment = getFragmentAtFrame();
        if (fragment instanceof AppsListFragment) {
          GeneralDialogCreation.showSortDialog((AppsListFragment) fragment, getAppTheme());
        }
        break;
      case R.id.sortby:
        if (ma != null) GeneralDialogCreation.showSortDialog(ma, getAppTheme(), getPrefs());
        break;
      case R.id.dsort:
        if (ma == null) return super.onOptionsItemSelected(item);
        String[] sort = getResources().getStringArray(R.array.directorysortmode);
        MaterialDialog.Builder builder = new MaterialDialog.Builder(mainActivity);
        builder.theme(getAppTheme().getMaterialDialogTheme());
        builder.title(R.string.directorysort);
        int current =
            Integer.parseInt(
                getPrefs().getString(PreferencesConstants.PREFERENCE_DIRECTORY_SORT_MODE, "0"));

        final MainFragment mainFrag = ma;

        builder
            .items(sort)
            .itemsCallbackSingleChoice(
                current,
                (dialog1, view, which, text) -> {
                  getPrefs()
                      .edit()
                      .putString(PreferencesConstants.PREFERENCE_DIRECTORY_SORT_MODE, "" + which)
                      .commit();
                  mainFrag.getSortModes();
                  mainFrag.updateList();
                  dialog1.dismiss();
                  return true;
                });
        builder.build().show();
        break;

      case R.id.search:
        getAppbar().getSearchView().revealSearchView();
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  /*@Override
  public void onRestoreInstanceState(Bundle savedInstanceState){
      COPY_PATH=savedInstanceState.getStringArrayList("COPY_PATH");
      MOVE_PATH=savedInstanceState.getStringArrayList("MOVE_PATH");
      oppathe = savedInstanceState.getString(KEY_OPERATION_PATH);
      oppathe1 = savedInstanceState.getString(KEY_OPERATED_ON_PATH);
      oparrayList = savedInstanceState.getStringArrayList(KEY_OPERATIONS_PATH_LIST);
      opnameList=savedInstanceState.getStringArrayList("opnameList");
      operation = savedInstanceState.getInt(KEY_OPERATION);
      selectedStorage = savedInstanceState.getInt(KEY_DRAWER_SELECTED, 0);
  }*/

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    // Sync the toggle state after onRestoreInstanceState has occurred.
    drawer.syncState();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig) {
    super.onConfigurationChanged(newConfig);
    // Pass any configuration change to the drawer toggls
    drawer.onConfigurationChanged(newConfig);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt(KEY_DRAWER_SELECTED, getDrawer().getDrawerSelectedItem());
    if (pasteHelper != null) {
      outState.putParcelable(PASTEHELPER_BUNDLE, pasteHelper);
    }

    if (oppathe != null) {
      outState.putString(KEY_OPERATION_PATH, oppathe);
      outState.putString(KEY_OPERATED_ON_PATH, oppathe1);
      outState.putParcelableArrayList(KEY_OPERATIONS_PATH_LIST, (oparrayList));
      outState.putInt(KEY_OPERATION, operation);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    unregisterReceiver(mainActivityHelper.mNotificationReceiver);
    unregisterReceiver(receiver2);

    if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
      unregisterReceiver(mOtgReceiver);
    }
    killToast();
  }

  @Override
  public void onResume() {
    super.onResume();
    if (materialDialog != null && !materialDialog.isShowing()) {
      materialDialog.show();
      materialDialog = null;
    }

    drawer.refreshDrawer();
    drawer.refactorDrawerLockMode();

    IntentFilter newFilter = new IntentFilter();
    newFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
    newFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
    newFilter.addDataScheme(ContentResolver.SCHEME_FILE);
    registerReceiver(mainActivityHelper.mNotificationReceiver, newFilter);
    registerReceiver(receiver2, new IntentFilter(TAG_INTENT_FILTER_GENERAL));

    if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
      updateUsbInformation();
    }
  }

  /** Updates everything related to USB devices MUST ALWAYS be called after onResume() */
  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  private void updateUsbInformation() {
    boolean isInformationUpdated = false;
    List<UsbOtgRepresentation> connectedDevices = OTGUtil.getMassStorageDevicesConnected(this);

    if (!connectedDevices.isEmpty()) {
      if (SingletonUsbOtg.getInstance().getUsbOtgRoot() != null
          && OTGUtil.isUsbUriAccessible(this)) {
        for (UsbOtgRepresentation device : connectedDevices) {
          if (SingletonUsbOtg.getInstance().checkIfRootIsFromDevice(device)) {
            isInformationUpdated = true;
            break;
          }
        }

        if (!isInformationUpdated) {
          SingletonUsbOtg.getInstance().resetUsbOtgRoot();
        }
      }

      if (!isInformationUpdated) {
        SingletonUsbOtg.getInstance().setConnectedDevice(connectedDevices.get(0));
        isInformationUpdated = true;
      }
    }

    if (!isInformationUpdated) {
      SingletonUsbOtg.getInstance().resetUsbOtgRoot();
      drawer.refreshDrawer();
    }

    // Registering intent filter for OTG
    IntentFilter otgFilter = new IntentFilter();
    otgFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
    otgFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
    registerReceiver(mOtgReceiver, otgFilter);
  }

  /** Receiver to check if a USB device is connected at the runtime of application */
  BroadcastReceiver mOtgReceiver =
      new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
            SingletonUsbOtg.getInstance().resetUsbOtgRoot();
            List<UsbOtgRepresentation> connectedDevices =
                OTGUtil.getMassStorageDevicesConnected(MainActivity.this);
            SingletonUsbOtg.getInstance().setConnectedDevice(connectedDevices.get(0));
            drawer.refreshDrawer();
          } else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
            SingletonUsbOtg.getInstance().resetUsbOtgRoot();
            drawer.refreshDrawer();
            goToMain(null);
          }
        }
      };

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_MENU) {
      /*
      ImageView ib = findViewById(R.id.action_overflow);
      if (ib.getVisibility() == View.VISIBLE) {
          ib.performClick();
      }
      */
      // return 'true' to prevent further propagation of the key event
      return true;
    }

    return super.onKeyDown(keyCode, event);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    // TODO: 6/5/2017 Android may choose to not call this method before destruction
    // TODO: https://developer.android.com/reference/android/app/Activity.html#onDestroy%28%29
    closeInteractiveShell();
    if (drawer != null && drawer.getBilling() != null) {
      drawer.getBilling().destroyBillingInstance();
    }
  }

  /** Closes the interactive shell and threads associated */
  private void closeInteractiveShell() {
    if (isRootExplorer()) {
      // close interactive shell and handler thread associated with it
      if (SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
        // let it finish up first with what it's doing
        handlerThread.quitSafely();
      } else handlerThread.quit();
      shellInteractive.close();
    }
  }

  public void updatePaths(int pos) {
    TabFragment tabFragment = getTabFragment();
    if (tabFragment != null) tabFragment.updatepaths(pos);
  }

  public MainFragment getCurrentMainFragment() {
    TabFragment tab = getTabFragment();

    if (tab != null && tab.getCurrentTabFragment() instanceof MainFragment) {
      return (MainFragment) tab.getCurrentTabFragment();
    } else return null;
  }

  public TabFragment getTabFragment() {
    Fragment fragment = getFragmentAtFrame();

    if (!(fragment instanceof TabFragment)) return null;
    else return (TabFragment) fragment;
  }

  public Fragment getFragmentAtFrame() {
    return getSupportFragmentManager().findFragmentById(R.id.content_frame);
  }

  public void setPagingEnabled(boolean b) {
    getTabFragment().mViewPager.setPagingEnabled(b);
  }

  public File getUsbDrive() {
    File parent = new File("/storage");

    try {
      for (File f : parent.listFiles())
        if (f.exists() && f.getName().toLowerCase().contains("usb") && f.canExecute()) return f;
    } catch (Exception e) {
    }

    parent = new File("/mnt/sdcard/usbStorage");
    if (parent.exists() && parent.canExecute()) return parent;
    parent = new File("/mnt/sdcard/usb_storage");
    if (parent.exists() && parent.canExecute()) return parent;

    return null;
  }

  public AppBar getAppbar() {
    return appbar;
  }

  public Drawer getDrawer() {
    return drawer;
  }

  protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
    super.onActivityResult(requestCode, responseCode, intent);
    if (requestCode == Drawer.image_selector_request_code) {
      drawer.onActivityResult(requestCode, responseCode, intent);
    } else if (requestCode == 3) {
      Uri treeUri;
      if (responseCode == Activity.RESULT_OK) {
        // Get Uri from Storage Access Framework.
        treeUri = intent.getData();
        // Persist URI - this is required for verification of writability.
        if (treeUri != null)
          getPrefs()
              .edit()
              .putString(PreferencesConstants.PREFERENCE_URI, treeUri.toString())
              .apply();
      } else {
        // If not confirmed SAF, or if still not writable, then revert settings.
        /* DialogUtil.displayError(getActivity(), R.string.message_dialog_cannot_write_to_folder_saf, false, currentFolder);
        ||!FileUtil.isWritableNormalOrSaf(currentFolder)*/
        return;
      }

      // After confirmation, update stored value of folder.
      // Persist access permissions.

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        getContentResolver()
            .takePersistableUriPermission(
                treeUri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
      }
      switch (operation) {
        case DataUtils.DELETE: // deletion
          new DeleteTask(mainActivity).execute((oparrayList));
          break;
        case DataUtils.SAVE_FILE:
          FileUtil.writeUriToStorage(
              this, urisToBeSaved, getContentResolver(), getCurrentMainFragment().getCurrentPath());
          urisToBeSaved = null;
          finish();
          break;
        default:
          LogHelper.logOnProductionOrCrash(TAG, "Incorrect value for switch");
      }
      operation = -1;
    } else if (requestCode == REQUEST_CODE_SAF) {
      if (responseCode == Activity.RESULT_OK && intent.getData() != null) {
        // otg access
        Uri usbOtgRoot = intent.getData();
        SingletonUsbOtg.getInstance().setUsbOtgRoot(usbOtgRoot);
        getCurrentMainFragment().loadlist(OTGUtil.PREFIX_OTG, false, OpenMode.OTG);
        drawer.closeIfNotLocked();
        if (drawer.isLocked()) drawer.onDrawerClosed();
      } else {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
        // otg access not provided
        drawer.resetPendingPath();
      }
    }
  }

  void initialisePreferences() {
    currentTab =
        getPrefs()
            .getInt(
                PreferencesConstants.PREFERENCE_CURRENT_TAB, PreferenceUtils.DEFAULT_CURRENT_TAB);
    @ColorInt
    int currentPrimary =
        ColorPreferenceHelper.getPrimary(getCurrentColorPreference(), MainActivity.currentTab);
    skinStatusBar = PreferenceUtils.getStatusColor(currentPrimary);
  }

  void initialiseViews() {
    appBarLayout = getAppbar().getAppbarLayout();

    // buttonBarFrame.setBackgroundColor(Color.parseColor(currentTab==1 ? skinTwo : skin));

    setSupportActionBar(getAppbar().getToolbar());

    drawer = new Drawer(this);

    indicator_layout = findViewById(R.id.indicator_layout);

    getSupportActionBar().setDisplayShowTitleEnabled(false);



    drawer.setDrawerHeaderBackground();
    // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor((currentTab==1
    // ? skinTwo : skin))));

    // status bar0
    if (SDK_INT == Build.VERSION_CODES.KITKAT_WATCH || SDK_INT == Build.VERSION_CODES.KITKAT) {
      SystemBarTintManager tintManager = new SystemBarTintManager(this);
      tintManager.setStatusBarTintEnabled(true);
      // tintManager.setStatusBarTintColor(Color.parseColor((currentTab==1 ? skinTwo : skin)));
      FrameLayout.MarginLayoutParams p =
          (ViewGroup.MarginLayoutParams) findViewById(R.id.drawer_layout).getLayoutParams();
      SystemBarTintManager.SystemBarConfig config = tintManager.getConfig();
      if (!drawer.isOnTablet()) p.setMargins(0, config.getStatusBarHeight(), 0, 0);
    } else if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      // window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      if (drawer.isOnTablet()) {
        window.setStatusBarColor((skinStatusBar));
      } else window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      if (getBoolean(PREFERENCE_COLORED_NAVIGATION)) window.setNavigationBarColor(skinStatusBar);
    }
  }

  /**
   * Call this method when you need to update the MainActivity view components' colors based on
   * update in the {@link MainActivity#currentTab} Warning - All the variables should be initialised
   * before calling this method!
   */
  public void updateViews(ColorDrawable colorDrawable) {
    // appbar view color
    appbar.getBottomBar().setBackgroundColor(colorDrawable.getColor());
    // action bar color
    mainActivity.getSupportActionBar().setBackgroundDrawable(colorDrawable);

    drawer.setBackgroundColor(colorDrawable.getColor());

    if (SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      // for lollipop devices, the status bar color
      mainActivity.getWindow().setStatusBarColor(colorDrawable.getColor());
      if (getBoolean(PREFERENCE_COLORED_NAVIGATION))
        mainActivity
            .getWindow()
            .setNavigationBarColor(PreferenceUtils.getStatusColor(colorDrawable.getColor()));
    } else if (SDK_INT == Build.VERSION_CODES.KITKAT_WATCH
        || SDK_INT == Build.VERSION_CODES.KITKAT) {

      // for kitkat devices, the status bar color
      SystemBarTintManager tintManager = new SystemBarTintManager(this);
      tintManager.setStatusBarTintEnabled(true);
      tintManager.setStatusBarTintColor(colorDrawable.getColor());
    }
  }



  public boolean copyToClipboard(Context context, String text) {
    try {
      android.content.ClipboardManager clipboard =
          (android.content.ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
      android.content.ClipData clip =
          android.content.ClipData.newPlainText("Path copied to clipboard", text);
      clipboard.setPrimaryClip(clip);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public void renameBookmark(final String title, final String path) {
    if (dataUtils.containsBooks(new String[] {title, path}) != -1) {
      RenameBookmark renameBookmark = RenameBookmark.getInstance(title, path, getAccent());
      if (renameBookmark != null) renameBookmark.show(getFragmentManager(), "renamedialog");
    }
  }

  public PasteHelper getPaste() {
    return pasteHelper;
  }

  public void setPaste(PasteHelper p) {
    pasteHelper = p;
  }

  @Override
  public void onNewIntent(Intent i) {
    super.onNewIntent(i);
    intent = i;
    path = i.getStringExtra("path");

    if (path != null) {
      if (new File(path).isDirectory()) {
        MainFragment ma = getCurrentMainFragment();
        if (ma != null) {
          ma.loadlist(path, false, OpenMode.FILE);
        } else goToMain(path);
      } else FileUtils.openFile(new File(path), mainActivity, getPrefs());
    } else if (i.getStringArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS) != null) {
      ArrayList<HybridFileParcelable> failedOps =
          i.getParcelableArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS);
      if (failedOps != null) {
        mainActivityHelper.showFailedOperationDialog(failedOps, this);
      }
    } else if ((openProcesses = i.getBooleanExtra(KEY_INTENT_PROCESS_VIEWER, false))) {
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      transaction.replace(
          R.id.content_frame, new ProcessViewerFragment(), KEY_INTENT_PROCESS_VIEWER);
      //   transaction.addToBackStack(null);
      openProcesses = false;
      // title.setText(utils.getString(con, R.string.process_viewer));
      // Commit the transaction
      transaction.commitAllowingStateLoss();
      supportInvalidateOptionsMenu();
    } else if (intent.getAction() != null) {
      checkForExternalIntent(intent);

      if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
        if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
          SingletonUsbOtg.getInstance().resetUsbOtgRoot();
          drawer.refreshDrawer();
        }
      }
    }
  }

  private BroadcastReceiver receiver2 =
      new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent i) {
          if (i.getStringArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS) != null) {
            ArrayList<HybridFileParcelable> failedOps =
                i.getParcelableArrayListExtra(TAG_INTENT_FILTER_FAILED_OPS);
            if (failedOps != null) {
              mainActivityHelper.showFailedOperationDialog(failedOps, mainActivity);
            }
          }
        }
      };




  @Override
  public void onHiddenFileAdded(String path) {
    utilsHandler.saveToDatabase(new OperationData(UtilsHandler.Operation.HIDDEN, path));
  }

  @Override
  public void onHiddenFileRemoved(String path) {
    utilsHandler.removeFromDatabase(new OperationData(UtilsHandler.Operation.HIDDEN, path));
  }

  @Override
  public void onHistoryAdded(String path) {
    utilsHandler.saveToDatabase(new OperationData(UtilsHandler.Operation.HISTORY, path));
  }

  @Override
  public void onBookAdded(String[] path, boolean refreshdrawer) {
    utilsHandler.saveToDatabase(
        new OperationData(UtilsHandler.Operation.BOOKMARKS, path[0], path[1]));
    if (refreshdrawer) drawer.refreshDrawer();
  }

  @Override
  public void onHistoryCleared() {
    utilsHandler.clearTable(UtilsHandler.Operation.HISTORY);
  }

  @Override
  public void delete(String title, String path) {
    utilsHandler.removeFromDatabase(
        new OperationData(UtilsHandler.Operation.BOOKMARKS, title, path));
    drawer.refreshDrawer();
  }

  @Override
  public void modify(String oldpath, String oldname, String newPath, String newname) {
    utilsHandler.renameBookmark(oldname, oldpath, newname, newPath);
    drawer.refreshDrawer();
  }

  @Override
  public void onPreExecute(String query) {
    getCurrentMainFragment().mSwipeRefreshLayout.setRefreshing(true);
    getCurrentMainFragment().onSearchPreExecute(query);
  }

  @Override
  public void onPostExecute(String query) {
    getCurrentMainFragment().onSearchCompleted(query);
    getCurrentMainFragment().mSwipeRefreshLayout.setRefreshing(false);
  }

  @Override
  public void onProgressUpdate(HybridFileParcelable val, String query) {
    getCurrentMainFragment().addSearchResult(val, query);
  }

  @Override
  public void onCancelled() {
    getCurrentMainFragment().reloadListElements(false, false, !getCurrentMainFragment().IS_LIST);
    getCurrentMainFragment().mSwipeRefreshLayout.setRefreshing(false);
  }

  @NonNull
  @Override
  public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
    return null;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
    if (data == null) {
      Toast.makeText(
              this,
              getResources().getString(R.string.cloud_error_failed_restart),
              Toast.LENGTH_LONG)
          .show();
      return;
    }

    /*
     * This is hack for repeated calls to onLoadFinished(),
     * we take the Cursor provided to check if the function
     * has already been called on it.
     *
     * TODO: find a fix for repeated callbacks to onLoadFinished()
     */
    if (cloudCursorData != null && cloudCursorData == data) return;
    cloudCursorData = data;

  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
    // For passing code check
  }

  /**
   * Invoke {@link FtpServerFragment#changeFTPServerPath(String)} to change FTP server share path.
   *
   * @see FtpServerFragment#changeFTPServerPath(String)
   * @see FolderChooserDialog
   * @see com.afollestad.materialdialogs.folderselector.FolderChooserDialog.FolderCallback
   * @param dialog
   * @param folder selected folder
   */
  @Override
  public void onFolderSelection(@NonNull FolderChooserDialog dialog, @NonNull File folder) {
  }

  /**
   * Do nothing other than dismissing the folder selection dialog.
   *
   * @see com.afollestad.materialdialogs.folderselector.FolderChooserDialog.FolderCallback
   * @param dialog
   */
  @Override
  public void onFolderChooserDismissed(@NonNull FolderChooserDialog dialog) {
    dialog.dismiss();
  }
}
